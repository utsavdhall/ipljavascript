# IPL Data Project- JavaScript

## Description

- This project aims to transform and display various statistics related to the IPL series.

## How to install

- Install node

        sudo apt install nodejs

- Install node package manager

        sudo apt install npm

- Clone the repository

        git clone https://gitlab.com/mountblue/24-1-python/utsav/javascript-ipl-project.git

## How to Use

- Run any of the files by using the node command, Example:

        node extra-runs-conceded.js

You can find the respective outputs in src/public/output in json files.

## Credits:

- Utsav Dhall

