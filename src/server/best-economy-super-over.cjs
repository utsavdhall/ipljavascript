const fs=require("fs");
const parse=require("csv-parser");

function bestEconomySuperOver(){
    let match=[];
    let topEconomicalRuns={};
    let topEconomicalBalls={};
    let topEconomicalBowler={};
    let inputDir="src/data/deliveries.csv";
    let outputDir="src/public/output/bestEconomySuperOver.json";
    fs.createReadStream(inputDir)
    .on("error", (error) => {
        console.log(error.message);
    })
    .pipe(
        parse(
            {
                delimiter:',',
                columns: true,
                ltrim:true
            }
        )
    )
    .on('data', (row) => {
        if(row["is_super_over"]=="1"){
            match.push(row);
        }
    })
    .on('end', () => {
        for(let rows of match){
            if(topEconomicalRuns[rows["bowler"]]!=undefined){
                topEconomicalRuns[rows["bowler"]]+=parseInt(rows["total_runs"]);
                topEconomicalBalls[rows["bowler"]]+=1;
            }
            else{
                topEconomicalRuns[rows["bowler"]]=parseInt(rows["total_runs"]);
                topEconomicalBalls[rows["bowler"]]=1;
            }
        }
        //console.log(topEconomicalRuns);
        //console.log(topEconomicalBalls);
        for(let players in topEconomicalBalls){
            topEconomicalBowler[players]=(topEconomicalRuns[players]/topEconomicalBalls[players]);
        }
        let sortedBowlers=[];
        for(let bowler in topEconomicalBowler){
            sortedBowlers.push([bowler,topEconomicalBowler[bowler]]);
        }
        sortedBowlers.sort((firstVal,secondVal)=>{
            return secondVal[1]-firstVal[1];
        });
        console.log(sortedBowlers);
        sortedBowlers=sortedBowlers.slice(0,10);
        topEconomicalBowler={};
        topEconomicalBowler[sortedBowlers[0][0]]=sortedBowlers[0][1];
        fs.writeFile(outputDir,
        JSON.stringify(topEconomicalBowler),(error)=>{
            if(error) throw error;
        })
    })
}

bestEconomySuperOver();