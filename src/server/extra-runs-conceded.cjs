const fs=require("fs");
const parse=require("csv-parser");

function extraRunsConceded(){
    let listOfValidId={}
    let extraRunsConcededData={};
    let inputDir1="src/data/matches.csv";
    let inputDir2="src/data/deliveries.csv";
    let outputDir="src/public/output/extraRunsConceded.json";
    let match=[];
    fs.createReadStream(inputDir1)
    .on("error", (error) => {
        console.log(error.message);
    })
    .pipe(
        parse(
            {
                delimiter:',',
                columns: true,
                ltrim:true
            }
        )
    )
    .on('data', (row) => {
        if(row.season=="2016"){
            listOfValidId[row.id]=1;
        }
    })
    .on('end', () => {
        //console.log(wonPerYearData);
        fs.createReadStream(inputDir2)
        .on("error", (error) => {
            console.log(error.message);
        })
        .pipe(
            parse(
                {
                    delimiter:',',
                    columns: true,
                    ltrim:true
                }
            )
        )
        .on('data', (row) => {
            if(row["match_id"] in listOfValidId){
                match.push(row);
            }
        })
        .on('end', () => {
            console.log(match);
            for(let rows of match){
                if(extraRunsConcededData[rows["bowling_team"]]!=undefined){
                    extraRunsConcededData[rows["bowling_team"]]+=parseInt(rows["extra_runs"]);
                }
                else{
                    extraRunsConcededData[rows["bowling_team"]]=parseInt(rows["extra_runs"]);
                }
            }
            //console.log(wonPerYearData);
            fs.writeFile(outputDir,
            JSON.stringify(extraRunsConcededData),(error)=>{
                if(error) throw error;
            })
        })
    })
}

extraRunsConceded();