const fs=require("fs");
const parse=require("csv-parse");
const getFilePath = require("../data/filepath.cjs");
const getOutputPath=require("./public/output/output-path.cjs");

function highestDismissal(){
    let match=[];
    let greatestCombinationDictionary={};
    let sortedListOfCombinations=[];
    let inputDir="src/data/deliveries.csv";
    let outputDir="src/public/output/highestDismissal.json";
    fs.createReadStream(inputDir)
    .on("error", (error) => {
        console.log(error.message);
    })
    .pipe(
        parse(
            {
                delimiter:',',
                columns: true,
                ltrim:true
            }
        )
    )
    .on('data', (row) => {
        match.push(row);
    })
    .on('end', () => {
        for(let matches of match){
            if(matches["player_dismissed"].length>0){
                if(greatestCombinationDictionary[matches["player_dismissed"]+" dismissed by "+matches["bowler"]]!=undefined){
                    greatestCombinationDictionary[matches["player_dismissed"]+" dismissed by "+matches["bowler"]]+=1;
                }
                else{
                    greatestCombinationDictionary[matches["player_dismissed"]+" dismissed by "+matches["bowler"]]=1;
                }
            }
        }
        for(let players in greatestCombinationDictionary){
            sortedListOfCombinations.push([players,greatestCombinationDictionary[players]]);
        }
        sortedListOfCombinations.sort((firstValue,secondValue)=>{
            return secondValue[1]-firstValue[1];
        });
        console.log(sortedListOfCombinations);
        let finalAns={};
        finalAns[sortedListOfCombinations[0][0]]=sortedListOfCombinations[0][1];
        fs.writeFile(outputDir,
        JSON.stringify(finalAns),(error)=>{
            if(error) throw error;
        })
    })
}

highestDismissal();