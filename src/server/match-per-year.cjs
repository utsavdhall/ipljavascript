const fs=require("fs");
const parse=require("csv-parser");

function matchesPerYear(){
    let match=[];
    let matchesPerYearData={};
    let inputDir1="src/data/matches.csv";
    let outputDir="src/public/output/matchesPerYear.json";
    fs.createReadStream(inputDir1)
    .on("error", (error) => {
        console.log(error.message);
    })
    .pipe(
        parse(
            {
                delimiter:',',
                columns: true,
                ltrim:true
            }
        )
    )
    .on('data', (row) => {
        match.push(row);
    })
    .on('end', () => {
        for(let rows of match){
            if(matchesPerYearData[rows["season"]]!=undefined){
                matchesPerYearData[rows["season"]]+=1;
            }
            else{
                matchesPerYearData[rows["season"]]=1;
            }
        }
        console.log(matchesPerYearData);
        fs.writeFile(outputDir,
        JSON.stringify(matchesPerYearData),(error)=>{
            if(error) throw error;
        })
    })
}

matchesPerYear();