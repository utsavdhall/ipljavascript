const fs=require("fs");
const parse=require("csv-parser");

function wonPerYear(){
    let match=[];
    let wonPerYearData={};
    let inputDir1="src/data/matches.csv";
    let outputDir="src/public/output/wonPerYear.json";
    fs.createReadStream(inputDir1)
    .on("error", (error) => {
        console.log(error.message);
    })
    .pipe(
        parse(
            {
                delimiter:',',
                columns: true,
                ltrim:true
            }
        )
    )
    .on('data', (row) => {
        match.push(row);
    })
    .on('end', () => {
        for(let rows of match){
            if(wonPerYearData[rows.winner]!=undefined){
                if(wonPerYearData[rows.winner][rows.season]!=undefined){
                    wonPerYearData[rows.winner][rows.season]+=1;
                }
                else{
                    wonPerYearData[rows.winner][rows.season]=1;
                }
            }
            else{
                wonPerYearData[rows.winner]={};
                wonPerYearData[rows.winner][rows.season]=1;
            }
        }
        console.log(wonPerYearData);
        fs.writeFile(outputDir,
        JSON.stringify(wonPerYearData),(error)=>{
            if(error) throw error;
        })
    })
}

wonPerYear();