const fs=require("fs");
const parse=require("csv-parser");

function playerMatchPerSeason(){
    let match=[];
    let playerOfTheMatchPerSeason={};
    let inputDir1="src/data/matches.csv";
    let outputDir="src/public/output/playerOfMatchEachSeason.json";
    fs.createReadStream(inputDir1)
    .on("error", (error) => {
        console.log(error.message);
    })
    .pipe(
        parse(
            {
                delimiter:',',
                columns: true,
                ltrim:true
            }
        )
    )
    .on('data', (row) => {
        match.push(row);
    })
    .on('end', () => {
        for(let matches of match){
            if(playerOfTheMatchPerSeason[matches["season"]]!=undefined){
                if(playerOfTheMatchPerSeason[matches["season"]][matches["player_of_match"]]!=undefined){
                    playerOfTheMatchPerSeason[matches["season"]][matches["player_of_match"]]+=1;
                }
                else{
                    playerOfTheMatchPerSeason[matches["season"]][matches["player_of_match"]]=1;
                }
            }
            else{
                playerOfTheMatchPerSeason[matches["season"]]={};
                playerOfTheMatchPerSeason[matches["season"]][matches["player_of_match"]]=1;
            }
        }
        let playerOfSeason={}
        let sortedPlayers=[]
        for(let season in playerOfTheMatchPerSeason){
            sortedPlayers=[];
            for(let players in playerOfTheMatchPerSeason[season]){
                sortedPlayers.push([players,playerOfTheMatchPerSeason[season][players]]);
            }
            sortedPlayers.sort((firstValue,secondValue)=>{
                return secondValue[1]-firstValue[1];
            })
            playerOfSeason[season]=sortedPlayers[0][0];
        }
        console.log(playerOfSeason);
        fs.writeFile(outputDir,
        JSON.stringify(playerOfSeason),(error)=>{
            if(error) throw error;
        })
    })
}

playerMatchPerSeason();