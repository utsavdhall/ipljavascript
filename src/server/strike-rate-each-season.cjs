const fs=require("fs");
const parse=require("csv-parser");

function strikeRateEachSeason(){
    let match=[];
    let strikeRate={};
    let idToSeason={};
    let numberOfRuns={};
    let numberOfBalls={};
    let inputDir1="src/data/matches.csv";
    let inputDir2="src/data/deliveries.csv";
    let outputDir="src/public/output/strikeRateEachSeason.json";
    //used import.meta.url because it wasn't letting me use relative paths.
    //please refer to "filepath.js" in the data folder
    fs.createReadStream(inputDir1)
    .on("error", (error) => {
        console.log(error.message);
    })
    .pipe(
        parse(
            {
                delimiter:',',
                columns: true,
                ltrim:true
            }
        )
    )
    .on('data', (row) => {
        idToSeason[row["id"]]=row["season"];
    })
    .on('end', () => {
        fs.createReadStream(inputDir2)
        .on("error", (error) => {
            console.log(error.message);
        })
        .pipe(
            parse(
                {
                    delimiter:',',
                    columns: true,
                    ltrim:true
                }
            )
        )
        .on('data', (row) => {
            match.push(row);
        })
        .on('end', () => {
            for(let matches of match){
                if(numberOfRuns[matches["batsman"]]!=undefined){
                    if(numberOfRuns[matches["batsman"]][idToSeason[matches["match_id"]]]!=undefined){
                        numberOfRuns[matches["batsman"]][idToSeason[matches["match_id"]]]+=parseInt(matches["total_runs"]);
                        numberOfBalls[matches["batsman"]][idToSeason[matches["match_id"]]]+=1;
                    }
                    else{
                        numberOfRuns[matches["batsman"]][idToSeason[matches["match_id"]]]=parseInt(matches["total_runs"]);
                        numberOfBalls[matches["batsman"]][idToSeason[matches["match_id"]]]=1;
                    }
                }
                else{
                    numberOfRuns[matches["batsman"]]={};
                    numberOfRuns[matches["batsman"]][idToSeason[matches["match_id"]]]=parseInt(matches["total_runs"]);
                    numberOfBalls[matches["batsman"]]={};
                    numberOfBalls[matches["batsman"]][idToSeason[matches["match_id"]]]=1;
                }
            }
            for(let players in numberOfRuns){
                for(let years in numberOfRuns[players]){
                    if(strikeRate[players]!=undefined){
                        strikeRate[players][years]=(numberOfRuns[players][years]/numberOfBalls[players][years])*100;
                    }
                    else{
                        strikeRate[players]={};
                        strikeRate[players][years]=(numberOfRuns[players][years]/numberOfBalls[players][years])*100;
                    }
                }
            }
            console.log(strikeRate);
            fs.writeFile(outputDir,
            JSON.stringify(strikeRate),(error)=>{
                if(error) throw error;
            })
        })
    })
}

strikeRateEachSeason();