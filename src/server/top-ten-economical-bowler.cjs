const fs=require("fs");
const parse=require("csv-parser");

function topTenEconomicalBowler(){
    let match=[];
    let topEconomicalBowler={};
    let listOfValidIds={};
    let topEconomicalRuns={};
    let topEconomicalBalls={};
    let inputDir1="src/data/matches.csv";
    let inputDir2="src/data/deliveries.csv";
    let outputDir="src/public/output/topTenEconomicalBowler.json";
    fs.createReadStream(inputDir1)
    .on("error", (error) => {
        console.log(error.message);
    })
    .pipe(
        parse(
            {
                delimiter:',',
                columns: true,
                ltrim:true
            }
        )
    )
    .on('data', (row) => {
        if(row["season"]=="2015"){
            listOfValidIds[row["id"]]=1;
        }
    })
    .on('end', () => {
        fs.createReadStream(inputDir2)
        .on("error", (error) => {
            console.log(error.message);
        })
        .pipe(
            parse(
                {
                    delimiter:',',
                    columns: true,
                    ltrim:true
                }
            )
        )
        .on('data', (row) => {
            if(row["match_id"] in listOfValidIds)
            {
                match.push(row);
            }
        })
        .on('end', () => {
            //console.log(listOfValidIds);
            for(let rows of match){
                if(topEconomicalRuns[rows["bowler"]]!=undefined){
                    topEconomicalRuns[rows["bowler"]]+=parseInt(rows["total_runs"]);
                    topEconomicalBalls[rows["bowler"]]+=1;
                }
                else{
                    topEconomicalRuns[rows["bowler"]]=parseInt(rows["total_runs"]);
                    topEconomicalBalls[rows["bowler"]]=1;
                }
            }
            //console.log(topEconomicalRuns);
            //console.log(topEconomicalBalls);
            for(let players in topEconomicalBalls){
                topEconomicalBowler[players]=(topEconomicalRuns[players]/topEconomicalBalls[players]);
            }
            let sortedBowlers=[];
            for(let bowler in topEconomicalBowler){
                sortedBowlers.push([bowler,topEconomicalBowler[bowler]]);
            }
            sortedBowlers.sort((firstVal,secondVal)=>{
                return secondVal[1]-firstVal[1];
            });
            sortedBowlers=sortedBowlers.slice(0,10);
            topEconomicalBowler={};
            for(let bowler of sortedBowlers){
                topEconomicalBowler[bowler[0]]=bowler[1];
            }
            console.log(sortedBowlers);
            console.log(topEconomicalBowler);
            fs.writeFile(outputDir,
            JSON.stringify(topEconomicalBowler),(error)=>{
                if(error) throw error;
            })
        })
    })
}

topTenEconomicalBowler();