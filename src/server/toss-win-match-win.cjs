const fs=require("fs");
const parse=require("csv-parser");
function tossWinMatchWin(){
    let match=[];
    let doubleWinners={};
    let inputDir="src/data/matches.csv";
    let outputDir="src/public/output/tossWinMatchWin.json";
    fs.createReadStream(inputDir)
    .on("error", (error) => {
        console.log(error.message);
    })
    .pipe(
        parse(
            {
                delimiter:',',
                columns: true,
                ltrim:true
            }
        )
    )
    .on('data', (row) => {
        match.push(row);
    })
    .on('end', () => {
        for(let matches of match){
            if(doubleWinners[matches["team1"]]==undefined){
                doubleWinners[matches["team1"]]=0;
            }
            if(doubleWinners[matches["team2"]]==undefined){
                doubleWinners[matches["team2"]]=0;
            }
            if(matches["toss_winner"]==matches["winner"]){
                doubleWinners[matches["toss_winner"]]+=1;
            }
            
        }
        console.log(doubleWinners);
        fs.writeFile(outputDir,
        JSON.stringify(doubleWinners),(error)=>{
            if(error) throw error;
        })
    })
}

tossWinMatchWin();